import { LitElement, html, css } from 'lit-element';

export class CvExperience extends LitElement {
  static get properties() {
    return {
      company: { type: String },
      from: { type: String },
      until: { type: String },
      jobTitle: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        display: block;
        padding: 24px;
        background-color: #66d9ff;
        margin: 8px;
      }

      .headline {
        display: flex;
        justify-content: flex-start;
        align-items: center;
      }

      .headline > h3 {
        flex-grow: 1;
      }

      .jobTitle {
        flex-grow: 1;
      }
    `;
  }

  render() {
    return html`
      <div class="headline">
        <h3>${this.company}</h3>
        <p class="jobTitle">${this.jobTitle}</p>
        <p class="date">${this.from} — ${this.until}</p>
      </div>
      <slot name="blurb"></slot>
    `;
  }
}
