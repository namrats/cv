import { LitElement, html, css } from 'lit-element';
import 'fa-icons';


export class CvSocial extends LitElement {
  static get properties() {
    return {
      medium: { type: String },
      url: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  render() {
    return html`
      <a href="${this.url}" target="_blank" rel="noopener noreferrer">
        <fa-icon class="${this._iconClass()}" color="${this._iconColor()}" size="2em"></fa-icon>
      </a>
    `;
  }

  _iconClass() {
    switch (this.medium) {
      case 'linkedin': return 'fab linkedin';
      case 'twitter': return 'fab twitter';
      case 'calendly': return 'far calendar';
    }
  }

  _iconColor() {
    switch (this.medium) {
      case 'linkedin': return '#0077B5';
      case 'twitter': return '#1DA1F2';
      case 'calendly': return '#393034';
    }
  }
}
