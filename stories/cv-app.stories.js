import { html } from 'lit-html';
import '../src/cv-app.js';

export default {
  title: 'cv-app',
};

export const App = () =>
  html`
    <style>
    @import url('https://fonts.googleapis.com/css?family=Anton|Raleway|Quicksand');
    </style>
    <cv-app></cv-app>
  `;
