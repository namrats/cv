import { LitElement, html, css } from 'lit-element';
import 'fa-icons';

export class CvAppearance extends LitElement {
  static get properties() {
    return {
      medium: { type: String },
      title: { type: String },
      setting: { type: String },
      date: { type: String },
      url: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        display: block;
      }

      .container {
        display: flex;
        margin: 8px;
        padding: 24px;
        justify-content: center;
        align-items: center;
        background-color: #66d9ff;
      }

      .container > fa-icon {
        width: 80px;
      }

      .container > h5 {
        width: 200px;
      }

      .container > a {
        width: 400px;
      }

      .container > p {
        width: 200px;
      }
    `;
  }

  render() {
    return html`
      <div class="container">
        <fa-icon class="${this._iconClass()}" color="black" size="48px"></fa-icon>
        <h5>${this.setting}</h5>
        <a href="${this.url}">${this.title}</a>
        <p>${this.date}</p>
      </div>
    `;
  }

  _iconClass() {
    switch (this.medium) {
      case 'talk': return `fab slideshare`;
      case 'meetup': return `fab meetup`;
      case 'podcast': return `fa podcast`
      default: return `fa question`;
    }
  }
}
