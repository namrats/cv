import { LitElement, html, css } from 'lit-element';
import './cv-social.js';
import './cv-skill.js';
import './cv-experience.js';
import './cv-appearance.js';

export class CvApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        min-height: 100vh;
        padding-top: 100px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        font-size: 14pt;
        font-family: Raleway;
        color: #1a2b42;
        margin: 0;
        scroll-behavior: smooth;
      }

      h1, h2 {
        font-family: Anton;
        font-weight: 400;
        color: #0077B5;
        text-align: center;
      }

      header {
        position: fixed;
        z-index: 100;
        top: 0;
        width: 100%;
        height: 100px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;

        background-color: #141a21;
        color: white;
        font-family: Quicksand;
      }

      header h1 {
        color: white;
        text-shadow: none;
      }

      header nav {
        display: flex;
        justify-content: center;
        align-items: center;
      }

      header nav > * {
        padding: .25rem 0;
        font-family: Quicksand;
        font-size: 14pt;
        font-weight: 700;
        display: block;

        color: white;
        text-decoration: none;
        background: none;
        border: 0px;
      }

      header nav > button {
        cursor: pointer;
      }

      header nav > * + * {
        margin-left: 1.5rem;
      }

      main, section {
        min-height: 100vh;
        width: 100%;
      }

      main > *, section > * {
        max-width: 1024px;
        margin-left: auto;
        margin-right: auto;
      }

      section:nth-child(odd) {
        background-color: #e6ffff;
      }

      section:nth-child(even) {
        background-color: #ccffff;
      }

      img.photo {
        float: left;
        height: 80vh;
      }

      ul.social {
        display: flex;
      }

      ul.social > li {
        flex-grow: 1;
        list-style: none;
        text-align: center;
      }

      div.gallery {
        display: flex;
        justify-content: space-around;
        align-items: flex-end;
      }

      div.gallery img {
        width: 400px;
        height: 250px;
        object-fit: cover;
        filter: grayscale(100%);
      }

      div.top-skills {
        display: flex;
        gap: 10px;
        margin-bottom: 20px;
      }

      div.top-skills > * {
        flex-grow: 1;
        min-width: 300px;
      }

      div.other-skills {
        display: flex;
        flex-wrap: wrap;
        gap: 10px;
      }

      div.other-skills > * {
        flex-grow: 1;
        min-width: 200px;
      }

      footer {
        font-size: calc(12px + 0.5vmin);
        align-items: center;
      }

      footer a {
        margin-left: 5px;
      }
    `;
  }

  render() {
    return html`
      <header>
        <h1>William Bartlett</h1>
        <nav>
          <button @click="${() => this._section('about')}">A propos de moi</button>
          <button href="#" @click="${() => this._section('skills')}">Compétences</button>
          <button href="#" @click="${() => this._section('xp')}">Expériences</button>
          <button href="#" @click="${() => this._section('side-projects')}">Projets parallèles</button>
          <button href="#" @click="${() => this._section('appearances')}">Conférences — Meetups — Podcasts</button>
          <button href="#" @click="${() => this._section('training')}">Formation</button>
          <cv-social medium="twitter" url="https://twitter.com/bartlettstarman"></cv-social>
          <cv-social medium="linkedin" url="https://www.linkedin.com/in/punkstarman/"></cv-social>
        </nav>
      </header>
      <main>
        <img class="photo" src="images/Photo-2019.jpg"/>
        <h1>William Bartlett</h1>
        <p>
          Je veux apporter à vos projets mon expérience en méthodes agiles, en DevOps et en facilitation, ainsi que mon
          savoir-faire en développement informatique.
        </p>
        <p>
          Je recherche un environnement stimulant, apprenant, inclusif et sincère
          à <strong>Rennes</strong> ou à <strong>Nantes</strong> où je peux m'épanouir dans un rôle de
          <strong>Consultant DevOps, Lead Tech ou CTO</strong>.
        </p>
        <p>
          J'aime également former mes pairs et partager avec la communauté au sens large à travers des meetups,
          des podcasts, des conférences et des projets open-source.
        </p>
      </main>
      <section id="about">
        <h2>A propos de moi</h2>
        <p>J'ai 35 ans. Je suis marié et ma femme et moi avons le privilège d'élever deux enfants.</p>
        <p>J'ai 12 ans d'expérience professionnelle.</p>
        <p>Je suis bilingue anglais-français. Je suis né aux Etats-Unis. Je vis en France depuis 1997.</p>
        <p>
          Je suis passionné de jeux de société. Je ne parle pas des classiques Monopoly ou Risk,
          mais plutôt des Catane, Carcassonne ou Root.
          <a href="https://boardgamegeek.com/user/bartlettstarman">Regardez ma collection</a>.
        </p>
        <p>
          J'aime les sports américains. J'ai pratiqué le baseball en club pendant 10 ans.
          Je prends généralement congé le lendemain du Super Bowl pour pouvoir regarder en direct malgré le décalage horaire.
        </p>
        <p>
          J'ai grandi dans une famille qui aime jouer de la musique et chaque membre joue plusieurs instruments.
          J'ai commencé par la flûte traversière, puis j'ai appris le piano, la guitare, la batterie.
          Maintenant, je m'amuse souvent avec mon ukulele ou ma mélodica.
        </p>
        <div class="gallery">
          <img src="images/baseball.jpg"/>
          <img src="images/Me4.jpg"/>
          <img src="images/ukulele.jpg"/>
        </div>
      </section>
      <section id="skills">
        <h2>Compétences</h2>
        <p>Je crois q'un·e développeur·euse 10x sait rendre 10 collègues plus productif·ve·s.</p>

        <div class="top-skills">
          <cv-skill name="Méthodes agiles">
            <p slot="blurb">
              Les méthodes agiles constituent une boîte à outils dans laquelle on pioche pour améliorer l'efficacité,
              la pertinence. Les méthodes comme XP, Scrum ou Kanban servent d'exemples mais il est rarement utile de
              les appliquer au pied de la lettre.
            </p>
          </cv-skill>
          <cv-skill name="DevOps">
            <p slot="blurb">
              Le DevOps s'inscrit dans la continuité des méthodes agiles. Il s'agit non seulement d'appliquer
              l'agilité aux équipes d'opérateurs, mais aussi d'améliorer la communication entre développeurs et
              opérateurs. Depuis le temps, cette approche s'est étendu à une collaboration plus saine entre tous
              les rôles de l'entreprise.
            </p>
          </cv-skill>
          <cv-skill name="Techniques de programmation">
            <p slot="blurb">
              Il y a celles et ceux qui savent <em>hacker</em> (bidouiller). Et puis il y a les personnes qui savent
              amener un produit jusqu'au bout d'une longue aventure pleine d'accomplissements, de découvertes et de
              rebondissements. J'aspire à être une personne qui a la jugeote pour appliquer la bonne méthode au bon
              moment.
            </p>
          </cv-skill>
        </div>
        <div class="other-skills">
          <cv-skill name="Facilitation"></cv-skill>
          <cv-skill name="Recrutement"></cv-skill>
          <cv-skill name="Git"></cv-skill>
          <cv-skill name="Java"></cv-skill>
          <cv-skill name="Python"></cv-skill>
          <cv-skill name="JavaScript"></cv-skill>
          <cv-skill name="C#"></cv-skill>
          <cv-skill name="Rust"></cv-skill>
          <cv-skill name="C"></cv-skill>
          <cv-skill name="HTML5/CSS3"></cv-skill>
          <cv-skill name="SQL"></cv-skill>
          <cv-skill name="MongoDB"></cv-skill>
          <cv-skill name="Docker"></cv-skill>
          <cv-skill name="Ansible"></cv-skill>
          <cv-skill name="Terraform"></cv-skill>
          <cv-skill name="Jenkins"></cv-skill>
          <cv-skill name="GitLab CI"></cv-skill>
          <cv-skill name="Web Components"></cv-skill>

          <cv-skill name="Test-Driven Development"></cv-skill>
          <cv-skill name="Domain-Driven Design"></cv-skill>
        </div>
      </section>
      <section id="xp">
        <h2>Expériences professionnelles</h2>
        <cv-experience company="Treeptik - Linkbynet" from="2016" jobTitle="Coach DevOps">
          <div slot="blurb">
            <h5>Coaching Agile/DevOps/Craftsmanship</h5>
            <p>
              Pour répondre à une demande d'expertise sur un projet ou une entreprise,
              j'audite les pratiques et technologies existantes et j'identifie les points de douleurs.
              Cela me permet d'énoncer des préconisations dans un rapport qui énumère les actions à mener
              (formations, coaching, preuves de concept, outils, technologie) pour atteindre les objectifs donnés.
              Après avoir priorisé ces actions avec le client, je participe à leur réalisation.
            </p>
            <p>
              Exemples :
              <ul>
                <li>Développement Python pour CloudEasier pendant 2 mois</li>
                <li>Coaching Agile et DevOps chez CMA CGM pendant 20 mois</li>
                <li>Audit DevOps chez ADSN pendant une semaine</li>
                <li>Accompagnement DevOps chez Linxo pendant 2 mois</li>
              </ul>
            </p>
            <p>
              Technologies : Docker, Java, JUnit, Maven, Python, Ansible, Terraform
            </p>

            <h5>Animation de formations</h5>
            <p>
              Elaboration du plan de formation, suivant les besoins du client. Animation de la formation via des cours,
              exercices pratiques et jeux.
            </p>

            <p>Exemples : Git, Maven, TDD, Spring Framework, méthodes agiles.</p>

            <h5>Activités non facturées</h5>
            <p>
              En parallèle de mes missions principales facturées, j'ai également participé aux activités
              de recrutement, d'avant-vente, d'amélioration continue interne et de R&D.
            </p>
            <p>
              Mes discussions avec différent·e·s candidat·e·s lors d'entretiens d'évaluation technique ont toujours été
              enrichissantes. C'est toujours un plaisir de pouvoir découvrir de nouveaux aspects de mon métier dans
              une atmosphère détendue qui permet de se laisser emporter au gré des méandres de la conversation.
              Même lorsque la relation avec le·a candidat·e ne se poursuit pas, le temps passé n'est pas perdu.
            </p>
          </div>
        </cv-experience>
        <cv-experience company="ALB" from="2012" until="2016" jobTitle="Consultant MOE/MOA">
          <div slot="blurb">
          <h5>En mission chez Dexia Crédit Local - SFIL</h5>
          <p>
            Maintenance et évolution d'un outil d'analyse financière des collectivités locales françaises permettant
            la saisie des données budgétaires, l'édition de dossiers de prospection et l'évaluation du risque de
            contrepartie. Mise en place d'une architecture en microservices.
          </p>
          <p>Technologies : C#, ASP.Net, Javascript, REST API, Spring, Selenium</p>

          <h5>En mission chez Crédit Coopératif</h5>
          <p>
            Développement d'un système d'information rassemblant et confrontant des données provenant des progiciels
            de gestion du Front-Office et du Back-Office de la salle des marchés. Utilisation de ces données pour
            produire des rapports répondant à des besoins règlementaires tels que l'ALM, le calcul du ratio McDonough,
            les accords Bâle III, la prévision de trésorerie.
          </p>
          <p>Technologies : Java, Spring, Hibernate, Oracle DB</p>
          </div>
        </cv-experience>
        <cv-experience company="CEA - CNAM" from="2009" until="2012" jobTitle="Candidat doctoral">
          <div slot="blurb">
            <h5>Abstraction et raffinement de modèles mémoire C dédiés à la preuve de programmes.</h5>
            <p>
              Description dans le langage Coq d'un modèle d'exécution du C paramétré par le niveau d'abstraction du tas.
              Travaux basés, entre autres, sur les projets CompCert, Jessie et ESC/Java.
              Application à la sûreté des systèmes embarqués dans l'aéronautique.
            </p>

            <p>Technologies : C, Coq, OCaml, Java</p>
          </div>
        </cv-experience>
      </section>
      <section id="side-projects">
        <h2>Projets parallèles</h2>
        <cv-experience company="serde-xml-rs" from="2019" jobTitle="Contributeur">
          <div slot="blurb">
            <p>
              Je contribue au projet open-source <a href="https://github.com/RReverser/serde-xml-rs/">serde-xml-rs</a>.
              Je revois et valide des pull requests en assurant un minimum de qualité.
              Je réponds aux issues ouverts en essayant d'orienter les utilisateurs de la bibliothèque vers des solutions
              lorsque c'est possible.
              Je publie les nouvelles versions sur <a href="https://crates.io/crates/serde-xml-rs">crates.io</a>.
            </p>

            <p>Technologies : Rust, Travis CI</p>
          </div>
        </cv-experience>
        <cv-experience company="Rustle - Build automation tool" from="2019" jobTitle="R&D">
          <div slot="blurb">
            <p>
              Chaque langage de programmation a besoin d'un certain outillage commun.
              Les étapes principales du cycle de publication d'un développement sont
              la récupération des dépendances, la construction de binaires,
              le lancement de tests et autres analyses,
              et la publication des binaires.
              L'ordonnancement des ces étapes varie peu d'un langage à un autre.
              Pourtant chaque communauté a progressivement réinventé la même roue en redécouvrant essentiellement
              les mêmes besoins, techniques, failles et optimisations.
              <a href="https://xkcd.com/927/"> Pourquoi ne pas mettre tous ces efforts en commun ?</a>
            </p>
            <p>
              En m'inspirant de l'état de l'art
              — comprenant des outils comme Bazel, Meson, Tup, Gradle, Maven, Poetry, Cargo —
              et en lisant des articles de recherche
              — notamment <a href="https://github.com/snowleopard/build/releases/download/icfp-final/build-systems.pdf">Build Systems à la Carte</a> —
              je construis peu à peu, de prototype en prototype, un système d'automatisation générique.
              Au passage je découvre des tas de problématiques liées à la livraison d'applications comme le
              <a href="https://reproducible-builds.org/"><em>Reproducible Build</em></a>
              et j'apprends différentes subtilités du langage Rust.
            </p>

            <p>Technologies : Rust, GitLab CI</p>
          </div>
        </cv-experience>
        <cv-experience company="Continuous Code" from="2019" jobTitle="chaîne YouTube">
          <div slot="blurb">
            <a href="https://www.youtube.com/channel/UCUseWQswSTyddznVKqL-2Jw">vidéos</a>
            <p>
              J'aimerais faire plus régulièrement des vidéos sur YouTube valorisant ma veille technologique.
              On trouve beaucoup de vidéos et de billets tutoriels mais peu qui expriment un avis argumentés
              sur les technologies et les conférences :
              des listes top 10, des revues avec points positifs et points négatifs, ...
            </p>
            <p>
              En fin d'année 2019, j'ai fait le test de quelques vidéos, mais j'ai dû consacrer mon temps à d'autres
              priorités. En 2021, j'aimerais reprendre la production d'une vidéo par mois.
            </p>
          </div>
        </cv-experience>
      </section>
      <section id="appearances">
        <h2>Conférences — Meetups — Podcasts</h2>
        <cv-appearance medium="talk" setting="Devoxx Belgium 2019" title="UX as an API" date="2019-11-07" url="https://noti.st/punkstarman/ek6O9P/ux-as-an-api"></cv-appearance>
        <cv-appearance medium="meetup" setting="Codeurs en Seine" title="Télétravail vs Bureaux - Table ronde" date="2020-06-30" url="https://www.codeursenseine.com/meetups/2020-06-16/table-ronde-teletravail-bureaux"></cv-appearance>
        <cv-appearance medium="podcast" setting="Electro Monk3ys" title="Gitops" date="2020-04-01" url="https://electro-monkeys.fr/?p=73"></cv-appearance>
        <cv-appearance medium="podcast" setting="If This Then Dev" title="Avant de coder, choisir le bon code" date="2020-02-05" url="https://ifttd.io/26-avant-de-coder-choisir-le-bon-code-william-bartlett/"></cv-appearance>
        <cv-appearance medium="talk" setting="SnowCamp 2020" title="ZeBattle : ORM vs Bare SQL" date="2020-01-24" url="https://noti.st/punkstarman/hydoYE/zebattle-orm-vs-bare-sql"></cv-appearance>
        <cv-appearance medium="talk" setting="APIdays Paris 2018" title="Just a Spoonful of Agile Helps the Transformation Go Down" date="2018-12-12" url="https://noti.st/punkstarman/arBmcn/just-a-spoonful-of-agile-helps-the-transformation-go-down"></cv-appearance>

        <p><a href="https://noti.st/punkstarman/">... et d'autres</a></p>
      </section>
      <section id="training">
        <h2>Formation</h2>
        <cv-experience company="ENSIIE" from="2004" until="2008" jobTitle="Diplôme d'ingénieur en informatique">
          <div slot="blurb">
            <p>Spécialisation en systèmes d'information, bases de données et développement formel.</p>
          </div>
        </cv-experience>
        <cv-experience company="Lycée Saint-Louis" from="2002" until="2004" jobTitle="Classes préparatoires MPSI-MP">
        </cv-experience>
      </section>
      <footer>
        <p>
          Made using
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://github.com/open-wc"
            >🚽 open-wc</a
          >.
        </p>
      </footer>
    `;
  }

  _section(id) {
    console.log('scrolling')
    const x = this.shadowRoot.getElementById(id).offsetTop - 100;
    window.scrollTo({top: x, behavior: 'smooth'});
    return false;
  }
}
