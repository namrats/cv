import { LitElement, html, css } from 'lit-element';

export class CvSkill extends LitElement {
  static get properties() {
    return {
      name: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        display: block;
        background-color: #66d9ff;
        padding: 0.2em;
        text-align: center;
      }
    `;
  }

  render() {
    return html`
      <h5>${this.name}</h5>
      <slot name="blurb"></slot>
    `;
  }
}
